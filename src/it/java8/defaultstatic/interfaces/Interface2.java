package it.java8.defaultstatic.interfaces;

public interface Interface2 {
	default void printMessage(String message){
		System.out.println(message);
	}
	
	static void printCiao(){
		System.out.println("Ciao");
	}
}
