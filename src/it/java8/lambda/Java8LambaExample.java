package it.java8.lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class Java8LambaExample {

	public static void main(String[] args) {
		List<Person> peopleList = new ArrayList<Person>();
		loadPeople(peopleList);
		Stream<Person> stream = peopleList.stream();
		stream.forEach(p -> {
			System.out.println(p.getCodiceFiscale());
			System.out.println(p.getNomeCognome());
		});
	}

	private static void loadPeople(List<Person> peopleList) {
		Person francesco = new Person();
		francesco.setCodiceFiscale("FLCFNC84R04H703X");
		francesco.setNomeCognome("Francesco Falcone");
		Person sabrina = new Person();
		sabrina.setCodiceFiscale("CRSSRN89M63A489O");
		sabrina.setNomeCognome("Sabrina Cristofaro");
		peopleList.add(francesco);
		peopleList.add(sabrina);
	}
	
	

}
