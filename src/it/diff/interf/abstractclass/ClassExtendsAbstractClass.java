package it.diff.interf.abstractclass;

public class ClassExtendsAbstractClass extends AbstractClass{

	@Override
	public void writeHello() {
		System.out.println("Hello from ClassExtendsAbstractClass");		
	}

}
