package it.diff.interf.abstractclass;

public abstract class AbstractClass {
	
	private String nome;
	
	public abstract void writeHello();
	
	public Integer getNum(){
		return new Integer(8);
	}
	
	public abstract void scrivi();

}
